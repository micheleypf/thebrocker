package GUI;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import pubsubfrontend.PcadClient;

import java.io.IOException;
import java.io.PrintStream;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;


public class Main extends Application {

    @FXML
    public TextField ServerNameField;
    public TextField hostNameField;
    public javafx.scene.control.TextArea textFlow;
    public Button connect_btn;
    public Button disconnectButton;
    public Button subscribeButton;
    public Label stateLabel;
    public Button closeButton;
    public Button hideButton;
    private static Stage mainStage;
    public Button pubBTN;
    public Button infoBxutton;
    public Button muteButton;
    public TextField muteTextfield;
    public Button unsubButton;
    public TextField unsubTextFIeld;


    private double xOffset = 0;
    private double yOffset = 0;

    private PcadClient client;
    private String currentHost;
    private String currentBroker;
    private PrintStream streamer;
    private boolean connected = false;

    static ArrayList<String> topics = new ArrayList<>();

    @Override
    public void start(Stage primaryStage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("scene.fxml"));
        primaryStage.setTitle("PCADbroker_Client");
        Scene scene = new Scene(root, 1024, 386);

        //Dragging window event Handlers
        root.setOnMousePressed(mouseEvent -> {
            xOffset = mouseEvent.getSceneX();
            yOffset = mouseEvent.getSceneY();
        });
        root.setOnMouseDragged(mouseEvent -> {
            primaryStage.setX(mouseEvent.getScreenX() - xOffset);
            primaryStage.setY(mouseEvent.getScreenY() - yOffset);
            primaryStage.setOpacity(0.8);
        });
        root.setOnMouseReleased(mouseEvent -> primaryStage.setOpacity(1));
        //

        primaryStage.setScene(scene);
        primaryStage.centerOnScreen();
        primaryStage.getIcons().add(new Image("file:/home/resonantFilter/IdeaProjects/thebrocker/src/GUI/icon.jpg"));
        mainStage = primaryStage;
        primaryStage.show();
    }

    public void connectButton(ActionEvent e) throws Exception {
        if (connected) {
            alertBox("warning",
                    "Server Connection",
                    "Trying Reconnection...",
                    "You are already connected to a Broker..."
            );
            return;
        }
        this.currentHost = this.hostNameField.getText();
        this.currentBroker = this.ServerNameField.getText();
        try {
            this.client = new PcadClient(currentHost, currentBroker);
            redirectStream();
            this.client.connect();
            alertBox("info",
                    "Server Connection",
                    "Client connected to Broker!",
                    "You successfully connected to "
                            + currentBroker + "@" + currentHost);
            this.textFlow.clear();
            message("Connected to " + currentBroker + "@" + currentHost);
            this.connected = true;
            this.stateLabel.setVisible(true);
        } catch (RemoteException e1) {
            alertBox("error",
                    "Server Connection",
                    "Couldn't connect to Broker " + currentBroker + "@" + currentHost,
                    "Error: Remote Exception");
        } catch (NotBoundException e1) {
            alertBox("error",
                    "Server Connection",
                    "Couldn't connect to Broker " + currentBroker + "@" + currentHost,
                    "Error: Not Bound Exception");
        }
    }

    public void disconnectButton(ActionEvent e) {
        if (!this.connected) {
            alertBox("info",
                    "Server Connection",
                    "Not currently Connected",
                    "You are trying to disconnect from a Broker Server," +
                            "but you are connected to none!");
            message("Available Servers: \n" +
                    "hostname: localhost\n" +
                    "server name: PCAD_1");
            return;
        }
        alertBox("warning",
                "Server Connection",
                "Disconnecting...",
                "You are now disconnecting from " +
                        this.currentBroker + "@" + this.currentHost);
        try {
            this.client.disconnect();
            this.connected = false;
            message("Disconnected from " + currentBroker + "@" + currentHost);
            Main.topics.clear();
            this.stateLabel.setVisible(false);
        } catch (RemoteException e1) {
            e1.printStackTrace();
        }
    }

    public void subscribeButton(ActionEvent e) {
        if (!this.connected) {
            alertBox("error", "Subscribe", "Not available right now",
                    "You need to be connected to a Broker in order to subscribe to topics");
            return;
        }
        try {
            Subscribe subscribeWindow = new Subscribe();
            subscribeWindow.setAndShow(this.client);
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void publishButton(ActionEvent e) {
        if (!this.connected) {
            alertBox("error", "Subscribe", "Not available right now",
                    "You need to be connected to a Broker in order to publish something");
        } else if (Main.topics.isEmpty()) {
            alertBox("warning", "Subscribe", "Not available right now",
                    "Subscribe to a Topic before start publishing");
        } else {
            try {
                Publish publishWindow = new Publish();
                publishWindow.setAndShow(this.client);
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }

    public void setUnsubButton(ActionEvent e) {
        if (!this.connected) {
            alertBox("error", "Unsubscribe", "Not available right now",
                    "You need to be connected to a Broker in order to subscribe or unsubscribe");
            return;
        } else if (Main.topics.isEmpty()) {
            alertBox("warning", "Subscribe", "Not available right now",
                    "You can't unsubscribe from nothing!\nPlease subscribe to a topic");
            return;
        }
        String input = this.unsubTextFIeld.getText();
        if (input.equals("")) {
            this.unsubTextFIeld.setText("Write Topic here");
        } else if (!Main.topics.contains(input)) {
            alertBox("warning",
                    "Unsubscribe",
                    "Couldn't unsubscribe from Topic " + input,
                    "You are not subscribed to " + input);
        } else {
            Task<Void> t = new Task<>() {
                @Override
                protected Void call() throws Exception {
                    client.unsubscribe(input);
                    Main.topics = client.finalTopicList().get();
                    return null;
                }
            };
            new Thread(t).start();
            alertBox("info",
                    "Subscribe",
                    "Unsubscribed from " + input,
                    "You unsuscribed from topic " + input + ", you won't be hearing from them anymore ;)");
            this.muteTextfield.clear();
        }
    }

    public void setMuteButton(ActionEvent e) {
        if (!this.connected) {
            alertBox("error", "Mute/Unmute", "Not available right now",
                    "You need to be connected to a Broker in order to mute/unmute a topic");
            return;
        } else if (Main.topics.isEmpty()) {
            alertBox("warning", "Subscribe", "Not available right now",
                    "You can't mute nothing!\nPlease subscribe to a topic");
            return;
        }

        String input = this.muteTextfield.getText();
        if (input.equals("")) {
            this.muteTextfield.setText("Write Topic here");
        } else if (!Main.topics.contains(input)) {
            alertBox("warning",
                    "Mute Topic",
                    "Couldn't mute topic",
                    "You are not subscribed to " + input);
        } else {
            Task<Void> t = new Task<>() {
                @Override
                protected Void call() {
                    client.mute(input);
                    return null;
                }
            };
            new Thread(t).start();
            alertBox("info",
                    "Mute/Unmute Topic",
                    "Ok.. If you say so... ;)",
                    null);
            this.muteTextfield.clear();

        }
    }

    private void redirectStream() {
        this.streamer = new PrintStream(new Console(this.textFlow));
        System.setOut(streamer);
        System.setErr(streamer);
    }

    static void alertBox(String type, String title, String header, String content) {
        Alert alertBox;
        switch (type) {
            default:
                alertBox = new Alert(Alert.AlertType.ERROR);
                alertBox.setHeaderText("Generic Error");
                alertBox.setContentText("Something went wrong...");
                Alert finalAlertBox = alertBox;
                Platform.runLater(() -> finalAlertBox.showAndWait());
            case "error":
                alertBox = new Alert(Alert.AlertType.ERROR);
                break;
            case "warning":
                alertBox = new Alert(Alert.AlertType.WARNING);
                break;
            case "info":
                alertBox = new Alert(Alert.AlertType.INFORMATION);
                break;
        }

        alertBox.setTitle(title);
        alertBox.setHeaderText(header);
        alertBox.setContentText(content);
        alertBox.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);

        Alert finalAlertBox = alertBox;
        Platform.runLater(() -> finalAlertBox.showAndWait());
    }

    void message(String s) {
        this.textFlow.appendText(s + "\n");
    }

    public void setHelpAndCredits() {
        alertBox("info", "Help & Credits", "PCADBroker_Client", helpAndCredits);
    }

    public void hideWindow() {
        mainStage.setIconified(true);
    }

    public void cleanExit() {
        if (connected) {
            try {
                client.disconnect();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        Platform.exit();
    }

    public static void main(String[] args) {
        launch(args);
        System.exit(0);
    }

    private final String helpAndCredits = "This is a simple Client based on PCAD_Broker architecture.\n" +
            "Fill \"Hostname\" and \"Server Name\" fields with your favourite " +
            "PCAD_Broker server. Once you are connected you will be " +
            "able to subscribe to new Topics and share your thoughts " +
            "with other subscribers. \n" +
            "You can also disconnect from one PCAD_Broker " +
            "so you can connect to another one. \n" +
            "Have fun!" +
            "\n" +
            "\n" +
            "Credits:\n" +
            "Michele Ferrari \n" +
            "Massimiliano Ciranni\n";
}
