package GUI;

import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import pubsubfrontend.PcadClient;

import java.rmi.RemoteException;


public class Subscribe {

    @FXML
    public TextField topicTextField;
    public Button okButton;
    public Button exitButton;

    private double xOffset;
    private double yOffset;

    private static String topic;
    private static Stage stage;
    private static PcadClient client;


    void setAndShow(PcadClient client) throws Exception {
        Subscribe.client = client;
        Parent root = FXMLLoader.load(getClass().getResource("subscribe.fxml"));
        stage = new Stage();
        Scene scene = new Scene(root, 350, 150);
        stage.setTitle("Subscribe");
        stage.initStyle(StageStyle.UNDECORATED);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setAlwaysOnTop(true);
        stage.setResizable(false);

        //Dragging window event Handlers
        root.setOnMousePressed(mouseEvent -> {
            xOffset = mouseEvent.getSceneX();
            yOffset = mouseEvent.getSceneY();
        });
        root.setOnMouseDragged(mouseEvent -> {
            stage.setX(mouseEvent.getScreenX() - xOffset);
            stage.setY(mouseEvent.getScreenY() - yOffset);
            stage.setOpacity(0.8);
        });
        root.setOnMouseReleased(mouseEvent -> stage.setOpacity(1));
        //

        stage.setScene(scene);
        stage.show();

    }

    public void setOkButton(ActionEvent e) {
        subscribe();
    }

    public void setEnterPressed(KeyEvent event) {
        if(event.getCode().equals(KeyCode.ENTER)) {
            subscribe();
        }
    }

    private void subscribe() {
        if (this.topicTextField.getText().equals("")) {
            this.topicTextField.setText("Topic's name here!!");
        } else {
            topic = this.topicTextField.getText();
            try {
                Main.topics = client.finalTopicList().get();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!Main.topics.contains(topic)) {
                final Task<Void> subscribeTask = new Task<Void>() {
                    @Override
                    protected Void call() throws Exception {
                        try {
                            client.subscribe(topic);
                            Main.topics.add(topic);
                        } catch (RemoteException e1) {
                            e1.printStackTrace();
                        }
                        return null;
                    }
                };
                new Thread(subscribeTask).start();
            } else {
                Main.alertBox("error",
                        "Subscribe",
                        "Already Subscribed",
                        "You are already subscribed to  \"" + topic + "\"");
            }
            stage.close();
        }
    }

    public void setExitButton(ActionEvent e) {
        stage.close();
    }
}