package GUI;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import pubsubfrontend.PcadClient;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Publish {


    private static String topic;
    private static String content;
    private static Stage stage;
    private static PcadClient client;
    public TextArea contentField;
    public Button publishButton;
    public Button exitButton;
    private boolean firstInteraction = true;

    private double xOffset;
    private double yOffset;

    void setAndShow(PcadClient client) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("publish.fxml"));
        Publish.client = client;
        stage = new Stage();
        Scene scene = new Scene(root, 600, 400);
        stage.setTitle("Publish");
        stage.initStyle(StageStyle.UNDECORATED);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.setScene(scene);

        //Dragging window event Handlers
        root.setOnMousePressed(mouseEvent -> {
            xOffset = mouseEvent.getSceneX();
            yOffset = mouseEvent.getSceneY();
        });
        root.setOnMouseDragged(mouseEvent -> {
            stage.setX(mouseEvent.getScreenX() - xOffset);
            stage.setY(mouseEvent.getScreenY() - yOffset);
            stage.setOpacity(0.8);
        });
        root.setOnMouseReleased(mouseEvent -> stage.setOpacity(1));

        stage.show();

        List<String> choices = new ArrayList<>(Main.topics);

        ChoiceDialog<String> dialog = new ChoiceDialog<>( choices.get(choices.size()-1), choices);
        dialog.initStyle(StageStyle.UNDECORATED);
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.setResizable(false);
        dialog.setTitle("Select Topic");
        dialog.setHeaderText("Which Topic do you want to Publish on?");
        dialog.setContentText("Choose Topic: ");

        Optional<String> result = dialog.showAndWait();
        if(!result.isPresent()) {
            stage.close();
        }
        result.ifPresent(choosenTopic -> Publish.topic = choosenTopic);
        stage.setAlwaysOnTop(true);

    }

    public void setOnShiftEnterPressed(KeyEvent event) {
        if(event.isShiftDown() && event.getCode().equals(KeyCode.ENTER)) {
            publish();
        }
    }

    public void setPublishButton(ActionEvent e) {
        publish();
    }

    private void publish() {
        if (contentField.getText().equals("")) {
            this.contentField.setText("Write something here!");
            return;
        }
        Publish.content = contentField.getText();

        if (!Main.topics.contains(Publish.topic)) {
            Publish.stage.setAlwaysOnTop(false);
            Main.alertBox("error", "Publish", "Couldn't Publish your Feed", "You are not subscribed to " + Publish.topic);
            return;
        }

        Task<Void> t = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                client.publish(Publish.topic, Publish.content);
                return null;
            }
        };
        Platform.runLater(t);
        stage.close();
    }


    public void setExitButton(ActionEvent e) {
        stage.close();
    }


    public void clearArea() throws InterruptedException {
        if (firstInteraction) {
            contentField.selectAll();
            firstInteraction = false;
        }
    }
}
