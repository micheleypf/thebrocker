package GUI;

import javafx.application.Platform;
import javafx.scene.control.TextArea;

import java.io.IOException;
import java.io.OutputStream;

public class Console extends OutputStream {

    private TextArea console;

    public Console(TextArea console) {
        this.console = console;
    }

    public void appendText(String text) {
        Platform.runLater(() -> this.console.appendText(text));
    }

    @Override
    public void write(int b) throws IOException {
        appendText(String.valueOf((char) b));
    }
}

