package brokertree;

import java.io.Serializable;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

public class Bulacco implements Serializable {
    private ConcurrentHashMap<String, RootNode> treeSet;

    public Bulacco() {
        this.treeSet = new ConcurrentHashMap<>();
    }

    private RootNode getRoot(String nodePath) {
        return this.treeSet.get(nodePath);
    }

    private void putRoot(String rootPath, RootNode rootNode) {
        this.treeSet.putIfAbsent(rootPath, rootNode);
    }

    public void delRoot(String rootPath) {
        this.treeSet.remove(rootPath);
    }

    public ConcurrentHashMap.KeySetView getKeys() {
        return this.treeSet.keySet();
    }

    //visitPathToNode visits the tree, from the root, following the given path. It returns the Node resulting from the
    //path. In case that the node does not exists, it will be created.
    public Node visitPathToNode(Iterator<Path> path) {
        if (!path.hasNext()) throw new IllegalArgumentException("No root specified");
        String rootName = path.next().toString();
        RootNode rootNode = getRoot(rootName);
        Node currentNode = rootNode;
        if (rootNode == null) {
            RootNode newRoot = new RootNode(rootName);
            putRoot(rootName, newRoot);
            currentNode = newRoot;
        }
        while (path.hasNext()) {
            String position = path.next().toString();
            Node childNode = currentNode.getChild(position);
            if (childNode == null) {
                StandardNode newNode = new StandardNode(position, currentNode);
                currentNode.putChild(newNode);
                childNode = newNode;
            }
            currentNode = childNode;
        }
        return currentNode;
    }

    //visitPathToNode returns the node corresponding the given string. This method uses strings as topics.
    //In case of null topic, it will be created.
    public Node visitPathToNode(String p) throws IllegalArgumentException {
        if (p == null) throw new IllegalArgumentException("No root specified");
        RootNode root = getRoot(p);
        Node cur = root;
        if (root == null) {
            RootNode newRoot = new RootNode(p, false);
            putRoot(p, newRoot);
            cur = newRoot;
        }
        return cur;
    }

    //visit returns the node corresponding the given string. This method uses strings as topics.
    public Node visit(String p) throws IllegalArgumentException {
        if (p == null) throw new IllegalArgumentException("No root specified");
        return getRoot(p);
    }
}
