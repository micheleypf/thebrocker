package brokertree;

import java.io.Serializable;
import java.util.concurrent.ConcurrentSkipListSet;

public interface Node extends Serializable {
    Node getParent(); //return the parent Node, null otherwise
    Node getChild(String key);
    boolean putChild(Node child);
    boolean isChildren(String Key);
    boolean putSub(String id);
    boolean putFeed(String text);
    boolean unsub(String id);
    Object[] getSubList();
    Object[] getFeedList();
    void setBadTopic();
    boolean getBadTopic();
    String getKey();

    ConcurrentSkipListSet<String> getSubscriberList();

    Integer getFeedCount();
}
