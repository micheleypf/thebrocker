package brokertree;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArrayList;

public class StandardNode implements Node {
    private final String Key;
    private final Node ParentNode;
    private boolean badTopic = false;
    private CopyOnWriteArrayList<Node> ChildrenList;
    private ConcurrentSkipListSet<String> SubscriberList;
    private ConcurrentLinkedQueue<String> FeedList;

    public StandardNode(String key, Node parentNode) {
        this.Key = key;
        this.ParentNode = parentNode;
        this.ChildrenList = new CopyOnWriteArrayList<>();
        this.FeedList = new ConcurrentLinkedQueue<>();
        this.SubscriberList = new ConcurrentSkipListSet<>();
    }

    public void setBadTopic() {
        this.badTopic = !this.badTopic;
    }

    public boolean getBadTopic() {
        return this.badTopic;
    }

    @Override
    public Node getParent() {
        return this.ParentNode;
    }

    @Override
    public Node getChild(String key) {
        for (Node it : this.ChildrenList) {
            if (it.getKey().equals(key)) {
                return it;
            }
        }
        return null;
    }

    @Override
    public boolean putChild(Node child) {
        return this.ChildrenList.addIfAbsent(child);
    }

    @Override
    public boolean isChildren(String key) {
        for (Node it : this.ChildrenList) {
            if (it.getKey().equals(key)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean putSub(String id) {
        return this.SubscriberList.add(id);
    }

    @Override
    public boolean putFeed(String text) {
        return this.FeedList.offer(text);
    }

    @Override
    public boolean unsub(String id) {
        return this.SubscriberList.remove(id);
    }

    @Override
    public Object[] getSubList() {
        return this.SubscriberList.toArray();
    }

    @Override
    public Object[] getFeedList() {
        return this.FeedList.toArray();
    }

    @Override
    public String getKey() {
        return this.Key;
    }

    @Override
    public ConcurrentSkipListSet<String> getSubscriberList() {
        return this.SubscriberList;
    }

    @Override
    public Integer getFeedCount() {
        return this.FeedList.size();
    }
}
