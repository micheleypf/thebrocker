import brokerbackend.PcadBroker;
import brokerbackend.Worker;
import cli.ArgsInput;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Optional;

public class thebroker {

    public static HashMap<String, String> getParsedValues(String path) throws IOException {
        HashMap<String, String> out = new HashMap<>();
        try (BufferedReader buf = new BufferedReader(new FileReader(path))) {
            String line;
            while ((line = buf.readLine()) != null) {
                if (line.contains("@")) {
                    String[] aline = line.split("@");
                    out.putIfAbsent(aline[0], aline[1]);
                } else {
                    System.out.println("Malformed line skipped: " + line);
                }
            }
        }
        return out;
    }

    public static void main(String[] args) {
        ArgsInput inArgs = new ArgsInput();
        HashMap<String, String> hosts = new HashMap<>();
        inArgs.setDefault("host", "localhost");
        inArgs.setDefault("name", "PCAD_0");
        inArgs.setDefault("worker", "true");
        inArgs.setDefault("th", "2");
        inArgs.setDefault("config", null);
        inArgs.setDefault("netc", "eth0");
        inArgs.parseArgs(args);

        Optional<String> file = Optional.ofNullable(inArgs.getValue("config"));
        if (file.isPresent()) {
            try {
                hosts = new HashMap<>(getParsedValues(file.get()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        PcadBroker broker = new PcadBroker(Integer.parseInt(inArgs.getValue("th")), inArgs.getValue("name"));
        try {
            broker.setup(inArgs.getValue("host"));
            if (Boolean.parseBoolean(inArgs.getValue("worker"))) {
                Worker worker = new Worker(broker, hosts, true, inArgs.getValue("netc"));
                worker.startWorker();
            }
        } catch (RemoteException | SocketException | UnknownHostException e) {
            e.printStackTrace();
        }
        System.out.println("System startup done...");
    }
}
