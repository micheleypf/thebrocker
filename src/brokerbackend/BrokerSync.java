package brokerbackend;

import pubsubfrontend.FrontendPubSub;

import java.rmi.RemoteException;

public interface BrokerSync extends FrontendPubSub {
    void connectTopic(String host, String name, String topic) throws RemoteException;
}
