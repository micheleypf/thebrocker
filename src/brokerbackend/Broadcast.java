package brokerbackend;

import java.rmi.RemoteException;

public class Broadcast implements Runnable {
    private String text;
    private PcadBroker broker;

    public Broadcast(String text, PcadBroker broker) {
        this.text = text;
        this.broker = broker;
    }

    @Override
    public void run() {
        this.broker.clientList.values().stream().forEach(it -> {
            try {
                it.send(this.text);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        });
    }
}
