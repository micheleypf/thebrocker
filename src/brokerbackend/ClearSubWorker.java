package brokerbackend;

import brokertree.Node;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

public class ClearSubWorker implements Runnable {

    private PcadBroker broker;
    private String deleteid = null;

    private Callable<Object[]> ping = () -> {
        ArrayList<String> deadClient = new ArrayList<>();
        for (String it : ClearSubWorker.this.broker.clientList.keySet()) {
            try {
                ClearSubWorker.this.broker.clientList.get(it).alive();
            } catch (RemoteException e) {
                ClearSubWorker.this.broker.clientList.remove(it);
                deadClient.add(it);
                System.out.println(it + " has been deleted from the client list, because of inactivity.");
            }
        }
        return deadClient.toArray();
    };

    public ClearSubWorker(PcadBroker b) {
        this.broker = b;
    }

    //If deleteID is specified, iw will be deleted the given ID without performing the ping check.
    public ClearSubWorker(PcadBroker b, String deleteID) {
        this.broker = b;
        this.deleteid = deleteID;
    }

    @Override
    public void run() {
        try {
            Object[] deadClients;
            if (this.deleteid != null) {
                deadClients = new Object[]{this.deleteid};
            } else {
                deadClients = (Object[]) this.broker.submitCall(ping).get();
            }
            for (Object o : this.broker.topics.getKeys()) {
                Node cur = this.broker.topics.visit((String) o);
                for (Object it : cur.getSubList()) {
                    for (Object str : deadClients) {
                        if (str.equals(it)) {
                            cur.unsub((String) it);
                        }
                    }
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
