package brokerbackend;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;

import static java.util.concurrent.Executors.newSingleThreadExecutor;

public class AutoDiscoverServer implements Runnable {

    private DatagramSocket socket;
    private ExecutorService exec;
    private ConcurrentHashMap<String, String> hosts;
    private boolean stopped = false;
    private InetAddress address;
    private String nodeName;

    AutoDiscoverServer(ConcurrentHashMap<String, String> pcadw, DatagramSocket sck, String nodeName,
                       InetAddress addr) {
        this.socket = sck;
        this.exec = newSingleThreadExecutor();
        this.hosts = pcadw;
        this.nodeName = nodeName;
        this.address = addr;
    }

    public void stopDiscoverServer() {
        this.stopped = true;
        this.exec.shutdown();
    }

    @Override
    public void run() {
        while (!stopped) {
            byte[] buf = new byte[256];
            DatagramPacket pck = new DatagramPacket(buf, buf.length);
            try {
                this.socket.receive(pck);
                exec.execute(new ParseDiscoverCookie(pck, hosts, nodeName));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
