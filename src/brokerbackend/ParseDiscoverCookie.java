package brokerbackend;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.util.concurrent.ConcurrentHashMap;

public class ParseDiscoverCookie implements Runnable {

    private DatagramPacket message;
    private ConcurrentHashMap<String, String> hostList;
    private String nodeName;

    ParseDiscoverCookie(DatagramPacket p, ConcurrentHashMap<String, String> hosts, String nodeName) {
        this.message = p;
        this.hostList = hosts;
        this.nodeName = nodeName;
    }

    @Override
    public void run() {
        try (ObjectInputStream objin = new ObjectInputStream(
                new ByteArrayInputStream(message.getData(), message.getOffset(), message.getLength()))) {
            DiscoverCookie cookie = (DiscoverCookie) objin.readObject();
            if (!cookie.getOrigin().equals(this.nodeName)) {
                if (this.hostList.putIfAbsent(cookie.getOrigin(), cookie.getMessage()) == null) {
                    System.out.println("The broker node " + cookie.getOrigin() + " has been found @" + cookie.getMessage());
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
