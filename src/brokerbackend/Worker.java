package brokerbackend;

import brokertree.Node;

import java.net.*;
import java.rmi.RemoteException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static java.util.concurrent.Executors.newScheduledThreadPool;

public class Worker {

    private PcadBroker broker;
    private Runnable clearTopics = () -> {
        for (Object it : broker.topics.getKeys()) {
            Node cur = this.broker.topics.visit((String) it);
            if (cur.getFeedList().length == 0 && !cur.getBadTopic()) {
                for (Object sub : cur.getSubList()) {
                    try {
                        this.broker.clientList.get(sub).warn(cur.getKey(), "This topic is empty, let's talk :)");
                    } catch (RemoteException e) {
                        System.out.println("Remote Exception: No path to client " + sub);
                    } catch (NullPointerException e) {
                        System.out.println("NullPointer Exception: No path to client " + sub);
                    }
                }
                cur.setBadTopic();
            } else if (cur.getFeedList().length == 0 && cur.getBadTopic()) {
                try {
                    this.broker.broadcast((String) it + " has been deleted, inactivity is a bitch!");
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                this.broker.topics.delRoot((String) it);
            } else if (cur.getSubList().length == 0) {
                this.broker.topics.delRoot((String) it);
            } else if (cur.getFeedList().length > 0 && cur.getBadTopic()) {
                cur.setBadTopic();
            }
        }
    };
    private ScheduledExecutorService exec;
    private ConcurrentHashMap<String, String> hosts;
    private AutoDiscoverServer discoverServer;
    private DatagramSocket socket;
    private final Optional<InetAddress> address;
    private String netCard;

    public Worker(PcadBroker broker, HashMap<String, String> map) {
        this.broker = broker;
        this.exec = newScheduledThreadPool(1);
        this.hosts = new ConcurrentHashMap<>(map);
        this.address = Optional.empty();
    }

    public Worker(PcadBroker broker, HashMap<String, String> map, boolean bcast, String networkCard)
            throws SocketException, UnknownHostException {
        this.broker = broker;
        this.exec = newScheduledThreadPool(1);
        this.hosts = new ConcurrentHashMap<>(map);
        this.netCard = networkCard;
        if (bcast) {
            this.address = Optional.ofNullable(getBroadCastAdrr());
            if (address.isPresent()) {
                System.out.println("UDP discover set @" + address.toString());
                this.socket = new DatagramSocket(9000);
                this.socket.setBroadcast(true);
                this.discoverServer = new AutoDiscoverServer(this.hosts, this.socket, this.broker.getId(),
                        this.address.get());
            }
        } else this.address = Optional.empty();
    }

    private InetAddress getBroadCastAdrr() throws SocketException {
        InetAddress addrs = null;
        Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
        while (interfaces.hasMoreElements()) {
            NetworkInterface i = interfaces.nextElement();
            if (!(i.isLoopback() || !i.isUp()) && i.getName().equals(this.netCard)) {
                addrs = i.getInterfaceAddresses().stream().map(it -> it.getBroadcast()).filter(Objects::nonNull)
                        .findAny().orElse(null);
            }
        }
        return addrs;
    }

    public void startWorker() {
        if (this.address.isPresent()) {
            this.exec.scheduleWithFixedDelay(new AutoDiscoverBeacon(this.socket,
                            new DiscoverCookie(this.broker.getId(), this.broker.getHost()), this.address.get()),
                    0, 1L, TimeUnit.MINUTES);
            new Thread(this.discoverServer, "UDP discover").start();
            System.out.println("UDP discover service launched...");
        }

        this.exec.scheduleWithFixedDelay(this.clearTopics, 4L, 4L, TimeUnit.MINUTES);
        this.exec.scheduleWithFixedDelay(new ClearSubWorker(this.broker), 2L, 2L, TimeUnit.MINUTES);
        this.exec.scheduleWithFixedDelay(new CompareTopicsWithRemote(this.broker, this.hosts),
                2L, 2L, TimeUnit.MINUTES);
        System.out.println("Worker launched...");
    }

    public void stopWorker() {
        this.socket.close();
        this.exec.shutdown();
    }
}
