package brokerbackend;

import java.rmi.RemoteException;

public class SendNotification implements Runnable {
    private String topic, text, id;
    private PcadBroker broker;

    public SendNotification(String topic, String text, String id, PcadBroker broker) {
        this.broker = broker;
        this.topic = topic;
        this.id = id;
        this.text = text;
    }

    @Override
    public void run() {
        Object[] sublist = this.broker.topics.visitPathToNode(this.topic).getSubList();
        for (Object it : sublist) {
            try {
                if (!it.equals(this.id)) {
                    this.broker.clientList.get(it).send(this.topic, this.text);
                } else {
                    this.broker.clientList.get(it).send("Feed Successfully Published on " + topic + "! " + text);
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }
}
