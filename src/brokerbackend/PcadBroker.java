package brokerbackend;

import brokertree.Bulacco;
import brokertree.Node;
import pubsubfrontend.FrontendPubSub;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import static java.util.concurrent.Executors.newFixedThreadPool;

public class PcadBroker implements BackendBroker {

    ConcurrentHashMap<String, FrontendPubSub> clientList;
    ExecutorService exec;
    Bulacco topics;
    private BrokerClient bc;
    private String id, host;

    public PcadBroker(int thNumber, String name) {
        super();
        this.id = name;
        this.clientList = new ConcurrentHashMap<>();
        this.topics = new Bulacco();
        this.exec = newFixedThreadPool(thNumber);
    }

    public void setup(String host) throws RemoteException {
        this.host = host;
        System.setProperty("java.rmi.server.hostname", host);
        this.bc = new BrokerClient(this.id, this);
        Registry reg = null;
        try {
            reg = LocateRegistry.createRegistry(8089);
        } catch (RemoteException e) {
            reg = LocateRegistry.getRegistry(8089);
        }
        BackendBroker stub = (BackendBroker) UnicastRemoteObject.exportObject(this, 0);
        reg.rebind(this.id, stub);
        System.out.println(this.id + "@" + host + " ready...");
    }

    public void broadcast(String content) throws RemoteException {
        this.exec.submit(new Broadcast(content, this));
    }

    public void submitTask(Runnable t) {
        this.exec.execute(t);
    }

    public Future submitCall(Callable t) {
        return this.exec.submit(t);
    }

    public String getId() {
        return this.id;
    }

    public void connectRemoteTopic(String h, String n, String t) throws RemoteException {
        this.bc.connectTopic(h, n, t);
    }

    @Override
    public void connect(FrontendPubSub it) throws RemoteException {
        this.clientList.putIfAbsent(it.getID(), it);
        System.out.println(it.getID() + " joined this broker.");
    }

    @Override
    public void disconnect(String id) throws RemoteException {
        this.clientList.remove(id);
        this.exec.submit(new ClearSubWorker(this, id));
        System.out.println(id + " left this broker.");
    }

    @Override
    public void subscribe(String id, String topic) throws RemoteException {
        this.topics.visitPathToNode(topic).putSub(id);
    }

    @Override
    public void unsubscribe(String id, String topic) throws RemoteException {
        this.topics.visitPathToNode(topic).unsub(id);
    }

    @Override
    public void publish(String id, String topic, String text) throws RemoteException {
        this.topics.visitPathToNode(topic).putFeed(text);
        this.exec.submit(new SendNotification(topic, text, id, this));
    }

    @Override
    public HashMap<String, Integer> getTopicList() throws RemoteException {
        HashMap<String, Integer> out = new HashMap<>();
        for (Object str : this.topics.getKeys()) {
            Node it = (Node) this.topics.visit((String) str);
            out.putIfAbsent(it.getKey(), it.getFeedCount());
        }
        return out;
    }

    public String getHost() {
        return host;
    }
}
