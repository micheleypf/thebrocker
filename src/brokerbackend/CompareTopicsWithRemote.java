package brokerbackend;

import brokertree.Node;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

public class CompareTopicsWithRemote implements Runnable {
    private PcadBroker broker;
    private ConcurrentHashMap<String, String> hosts;

    public CompareTopicsWithRemote(PcadBroker b, ConcurrentHashMap<String, String> map) {
        this.broker = b;
        this.hosts = map;
    }

    @Override
    public void run() {
        for (String it : this.hosts.keySet()) {
            try {
                Registry reg = LocateRegistry.getRegistry(this.hosts.get(it), 8089);
                BackendBroker stub = (BackendBroker) reg.lookup(it);
                HashMap<String, Integer> rt = stub.getTopicList();
                for (String str : rt.keySet()) {
                    Node cur = this.broker.topics.visit(str);
                    if (cur != null && cur.getFeedCount() < rt.get(str)) {
                        this.broker.submitTask(() -> {
                            try {
                                this.broker.connectRemoteTopic(this.hosts.get(it), it, cur.getKey());
                            } catch (RemoteException e) {
                                System.out.println("Remote topic request, no path to: " + it);
                                e.printStackTrace();
                            }
                        });
                    }
                }
            } catch (RemoteException e) {
                System.out.println("Remote topic request, no path to: " + it);
            } catch (NotBoundException e) {
                System.out.println("Remote topic request, no path to: " + it);
            }
        }
    }
}
