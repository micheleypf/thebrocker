package brokerbackend;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class AutoDiscoverBeacon implements Runnable {

    private DatagramSocket socket;
    private DiscoverCookie cookie;
    private InetAddress address;

    AutoDiscoverBeacon(DatagramSocket sck, DiscoverCookie ck, InetAddress addr) {
        this.socket = sck;
        this.cookie = ck;
        this.address = addr;
    }

    @Override
    public void run() {
        try (ByteArrayOutputStream bty = new ByteArrayOutputStream();
             ObjectOutputStream objo = new ObjectOutputStream(bty)) {
            objo.writeObject(this.cookie);
            byte[] togo = bty.toByteArray();
            DatagramPacket pulse = new DatagramPacket(togo, togo.length, this.address, 9000);
            this.socket.send(pulse);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
