package brokerbackend;

import pubsubfrontend.FrontendPubSub;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.HashMap;

public interface BackendBroker extends Remote, Serializable {
    void connect(FrontendPubSub it) throws RemoteException;
    void disconnect(String id) throws RemoteException;
    void subscribe(String id, String topic) throws RemoteException;
    void unsubscribe(String id, String topic) throws RemoteException;
    void publish(String id, String topic, String text) throws RemoteException;

    HashMap<String, Integer> getTopicList() throws RemoteException;
}
