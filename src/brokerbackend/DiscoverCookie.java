package brokerbackend;

import java.io.Serializable;

import static java.util.Objects.requireNonNull;

public class DiscoverCookie implements Serializable {

    private String brokerOrigin;
    private String message;

    public DiscoverCookie(String origin, String m) {
        this.brokerOrigin = requireNonNull(origin);
        this.message = requireNonNull(m);
    }

    public String getMessage() {
        return message;
    }

    public String getOrigin() {
        return brokerOrigin;
    }
}
