package brokerbackend;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.concurrent.ConcurrentSkipListSet;

public class BrokerClient extends UnicastRemoteObject implements BrokerSync {
    private String id;
    private PcadBroker broker;
    private ConcurrentSkipListSet<String> connectedHost;

    public BrokerClient(String id, PcadBroker t) throws RemoteException {
        super();
        this.id = id + "_bc";
        this.broker = t;
        this.connectedHost = new ConcurrentSkipListSet<>();
    }

    public String getClientId() {
        return this.id;
    }

    @Override
    public String getID() throws RemoteException {
        return this.id;
    }

    @Override
    public void send(String topic, String text) throws RemoteException {
        this.broker.topics.visitPathToNode(topic).putFeed(text);
        this.broker.exec.submit(new SendNotification(topic, text, "", this.broker));
    }

    @Override
    public void send(String notification) throws RemoteException {
        this.broker.exec.submit(new Broadcast(notification, this.broker));
    }

    @Override
    public void warn(String topic, String text) throws RemoteException {
        System.out.println("Remote warning -> " + topic + " :" + text);
    }

    @Override
    public boolean alive() throws RemoteException {
        return true;
    }

    @Override
    public void connectTopic(String host, String name, String topic) throws RemoteException {
        Registry registry = LocateRegistry.getRegistry(host, 8089);
        try {
            BackendBroker stub = (BackendBroker) registry.lookup(name);
            stub.connect(this);
            stub.subscribe(this.id, topic);
            this.connectedHost.add(name);
        } catch (NotBoundException e) {
            e.printStackTrace();
        }
    }

}
