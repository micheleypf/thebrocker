package pubsubfrontend;

import brokerbackend.BackendBroker;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.*;

public class PcadClient extends UnicastRemoteObject implements FrontendPubSub {
    private BackendBroker brkr;
    private ConcurrentHashMap<String, Boolean> topicList;
    private final String id, remoteHost, serverName;

    public PcadClient(String rmH, String rmN) throws RemoteException {
        super();
        this.id = UUID.randomUUID().toString();
        this.remoteHost = rmH;
        this.serverName = rmN;
        this.topicList = new ConcurrentHashMap<>();
    }

    private void setup() throws RemoteException, NotBoundException {
        Registry registry = LocateRegistry.getRegistry(this.remoteHost, 8089);
        this.brkr = (BackendBroker) registry.lookup(this.serverName);
    }


    public void mute(String topic) {
        this.topicList.replace(topic, !this.topicList.get(topic));
    }

    public void connect() throws RemoteException, NotBoundException {
        setup();
        this.brkr.connect(this);
    }

    public void disconnect() throws RemoteException {
        this.brkr.disconnect(this.id);
    }

    public void subscribe(String topic) throws RemoteException {
        this.topicList.putIfAbsent(topic, true);
        this.brkr.subscribe(this.id, topic);
    }

    public void unsubscribe(String topic) throws RemoteException {
        this.topicList.remove(topic);
        this.brkr.unsubscribe(this.id, topic);
    }

    public void publish(String topic, String text) throws RemoteException {
        this.brkr.publish(this.id, topic, text);
    }

    public HashMap<String, Integer> getTopicList() throws RemoteException {
        return this.brkr.getTopicList();
    }

    @Override
    public void send(String topic, String text) throws RemoteException {
        try {
            if (this.topicList.get(topic)) {
                System.out.println(topic + ": " + text);
            }
        } catch (NullPointerException e) {
            System.out.println("No topic: " + topic);
        }
    }

    public Future<ArrayList<String>> finalTopicList() throws Exception {
        ExecutorService e = Executors.newSingleThreadExecutor();
        Callable<ArrayList<String>> listCallable = () -> new ArrayList<>(this.topicList.keySet());
        return e.submit(listCallable);
    }

    @Override
    public void send(String notification) {
        System.out.println("#!#: " + notification);
    }

    @Override
    public boolean alive() throws RemoteException {
        System.out.println("Ping request received from broker.");
        return true;
    }

    @Override
    public void warn(String topic, String text) throws RemoteException {
        System.err.println(topic + ": +++" + text.toUpperCase() + " +++");
    }

    @Override
    public String getID() {
        return this.id;
    }
}
