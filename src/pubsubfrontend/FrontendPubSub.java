package pubsubfrontend;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface FrontendPubSub extends Remote, Serializable {
    String getID() throws RemoteException;
    void send(String topic, String text) throws RemoteException;

    void send(String notification) throws RemoteException;
    void warn(String topic, String text) throws RemoteException;

    boolean alive() throws RemoteException;
}