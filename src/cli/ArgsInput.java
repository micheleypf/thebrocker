package cli;

import java.util.HashMap;

public class ArgsInput {
    private HashMap<String, String> argsmap, defaults;

    public ArgsInput() {
        this.argsmap = new HashMap<>();
        this.defaults = new HashMap<>();
    }

    public void setDefault(String k, String v) {
        this.defaults.putIfAbsent(k, v);
    }

    public void parseArgs(String[] args) {
        for (int i = 0; i < args.length; ++i) {
            if (args[i].matches("-{1,2}[a-zA-Z]*\\b")) {
                this.argsmap.putIfAbsent(args[i].replaceAll("-", ""), args[++i]);
            }
        }
    }

    public String getValue(String k) throws IllegalArgumentException {
        if (this.argsmap.containsKey(k)) {
            return this.argsmap.get(k);
        } else if (this.defaults.containsKey(k)) {
            return this.defaults.get(k);
        } else throw new IllegalArgumentException("Argument not set.");
    }
}
